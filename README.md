# file-sharing

stuff

## ulratdb

debian-based mssql server for ulratdb, estimated size 1.37GB

### steps:
1. install docker
2. build
```
docker build . -t ulratdb
```
3. run
```
docker run -ti -p 1433:1433 ulratdb
```
(wait ~ 10 seconds for mssql-server service to start)

4. connect
```
username : SA
password : Sa!12345
```
### connect using sqlcmd and create ULRATDB
```
sqlcmd -S localhost -U SA -P 'Sa!12345' -Q 'CREATE DATABASE ULRATDB'
```
### connection string in config.json:

```
"Data Source=localhost,1433\\SQLEXPRESS;Database=ULRATDB;User ID=SA;Password=Sa!12345"
```