#Download base image
FROM debian:10-slim

RUN  mkdir /tmp/lib
COPY lib /tmp/lib

# Install sudo and add user docker
RUN dpkg -i /tmp/lib/lsb-base_10.2019051400_all.deb \
	&& dpkg -i /tmp/lib/sudo_1.8.27-1+deb10u2_amd64.deb \
	&& (useradd -ms /bin/bash docker && echo "docker:docker" | chpasswd) \
	&& (echo "root ALL=(ALL:ALL) NOPASSWD: ALL" | tee /etc/sudoers.d/root)\
	&& (echo "docker ALL=(ALL:ALL) NOPASSWD: ALL" | tee /etc/sudoers.d/docker)\
    && rm -rf /var/lib/apt/lists/*

USER docker
WORKDIR /home/docker

# Install mssql server dependencies (old packages from debian jessie)
RUN sudo dpkg -i /tmp/lib/multiarch-support_2.19-18+deb8u10_amd64.deb \
	&& sudo DEBIAN_FRONTEND=noninteractive dpkg -i /tmp/lib/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb \
	&& sudo dpkg -i /tmp/lib/libjemalloc1_3.6.0-3_amd64.deb \
	&& sudo rm -r /tmp/lib

# Wget & Repository (from ubuntu because there is no official releas for debian yet)
RUN sudo apt-get update && sudo apt-get -y install apt-utils wget gnupg2 \
    && (wget -qO- https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -) \
    && (wget -qO- https://packages.microsoft.com/config/ubuntu/18.04/mssql-server-2017.list | sudo tee /etc/apt/sources.list.d/mssql-server-2017.list) \
    && (wget -qO- https://packages.microsoft.com/config/ubuntu/18.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list)

# Install MsSqlServer	
RUN sudo apt-get update && sudo apt-get -y install mssql-server \
	; sudo MSSQL_PID=Express ACCEPT_EULA=Y MSSQL_SA_PASSWORD='Sa!12345' /opt/mssql/bin/mssql-conf -n setup \
	; sudo rm -rf /var/lib/apt/lists/*

# Pseudo systemctl because systemd cannot run well on docker
RUN sudo wget https://github.com/gdraheim/docker-systemctl-replacement/raw/master/files/docker/systemctl.py -O /usr/bin/systemctl \
	&& echo "/bin/bash | sudo /usr/bin/python /usr/bin/systemctl restart mssql-server.service" | tee /home/docker/startup.sh

# Start MsSqlServer
CMD ["/bin/bash", "/home/docker/startup.sh"]

# (WIP) Create ULRATDB, you are pleased to edit :>
# RUN sudo apt-get update && sudo apt-get -y install mssql-server mssql-tools unixodbc-dev \
#	&& /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'Sa!12345' -Q 'CREATE DATABASE ULRATDB' \
#	&& sudo rm -rf /var/lib/apt/lists/*